#!/usr/bin/env bash
# Original file from Girl Life ecv - Cat

set -x

# Set those lines to fit your setup.
# This is where glife.qsp will be copied. If you don't want to move it just comment (#) the line below.
#DESTDIR=../SOB

# The file that will be generated or open
QSPFILE=MBF_mod.qsp

#######################################################################

./txtmerge.py main MBF_mod.txt
if [[ "$OSTYPE" == "linux-gnu" ]]; then
	./txt2gam.linux MBF_mod.txt "${QSPFILE}" 1> /dev/null
elif [[ "$OSTYPE" == "darwin"* ]]; then
	./txt2gam.mac MBF_mod.txt "${QSPFILE}" 1> /dev/null
elif [[ "$OSTYPE" == "msys" ]]; then
	if [[ "$MSYSTEM_CARCH" == "x86_64" ]]; then
		./txt2gam64.exe MBF_mod.txt "${QSPFILE}" 1> /dev/null
	else
		./txt2gam.exe MBF_mod.txt "${QSPFILE}" 1> /dev/null
	fi
fi
if [ -d "${DESTDIR}" ]; then
	cp --reflink=auto "${QSPFILE}" "${DESTDIR}"
fi

